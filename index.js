//alert("B249!");

//create student one
let studentOneName = 'John';
let studentOneEmail = 'john@mail.com';
let studentOneGrades = [89, 84, 78, 88];


//create student two
let studentTwoName = 'Joe';
let studentTwoEmail = 'joe@mail.com';
let studentTwoGrades = [78, 82, 79, 85];

//create student three
let studentThreeName = 'Jane';
let studentThreeEmail = 'jane@mail.com';
let studentThreeGrades = [87, 89, 91, 93];


//create student four
let studentFourName = 'Jessie';
let studentFourEmail = 'jessie@mail.com';
let studentFourGrades = [91, 89, 92, 93];


function login (email) {
	console.log(`${email} has logged in`);
}

login(studentOneEmail)

function logout (email) {
	console.log(`${email} has logged out`);
}


function listGrades(grades) {
	grades.forEach(grade => {
		console.log(grade)
	})
}

// spaghetti code - code that is poorly organized that it becomes challenging to work with

// Use an object literal: {}
// Encapsulation - the organization of information (properties) and behavior (as methods) to belong to the object that enacapsulate them (the scope of encapsulation is denoted by the object literals)

let studentOne = {
	name: "John",
	email: "john@mail.com",
	grades: [89, 84, 78, 88],
	// add the functionalities available to a student as object methods
	// the keyword "this" refers to the object encapsulating the method where "this" is called
	// "this" will refer to the global window object when called outside the object
	login () {
		console.log(`${this.email} has logged in`);
	},
	logout () {
		console.log(`${this.email} has logged out`);
	},
	listGrades () {
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
		// return this.grades;
	},
	computeAve (){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		// console.log(`${this.name}'s quarterly average is:`);
		return sum/(this.grades.length)
	},
	willPass (){
		return this.computeAve() >= 85;
	},
	willPassWithHonors (){
		return (this.willPass() && this.computeAve() >= 90);
	}
}

console.log(`Student One's name is ${studentOne.name}`);
console.log(`Student One's email is ${studentOne.email}`);
console.log(`Student One's grades are ${studentOne.grades}`);


// Mini Exercise

/*

Create a function that will compute the quartertly average of the studentOne's grades
**done above**

*/


// Mini Exercise 2

/*

Create a function that will return true if the average grade is greater than or equal to 85
**done above**

*/

// Mini Exercise 3

/*

Create a function called willPassWithHonors that returns true if the student has passed AND their average is >=90
**done above**

*/

// Activity Quiz

/*

1. Spaghetti Code
2. {}
3. Encapsulation
4. studentOne.enroll()
5. true
6. {key1: value1, key2: value2}
7. true
8. true
9. true
10. true

*/

// Activity Function Coding

// 1. - 4.

let studentTwo = {
	name: "Joe",
	email: "joe@mail.com",
	grades: [78, 82, 79, 85],
	login () {
		console.log(`${this.email} has logged in`);
	},
	logout () {
		console.log(`${this.email} has logged out`);
	},
	listGrades () {
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
	},
	computeAve (){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		return sum/(this.grades.length)
	},
	willPass (){
		return this.computeAve() >= 85;
	},
	willPassWithHonors (){
		if (this.computeAve() >= 90) {
			return true
		} else if (this.computeAve() >= 85) {
			return false
		} else {
			return undefined
		}
	}
}


let studentThree = {
	name: "Jane",
	email: "jane@mail.com",
	grades: [87, 89, 91, 93],
	login () {
		console.log(`${this.email} has logged in`);
	},
	logout () {
		console.log(`${this.email} has logged out`);
	},
	listGrades () {
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
	},
	computeAve (){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		return sum/(this.grades.length)
	},
	willPass (){
		return this.computeAve() >= 85;
	},
	willPassWithHonors (){
		if (this.computeAve() >= 90) {
			return true
		} else if (this.computeAve() >= 85) {
			return false
		} else {
			return undefined
		}
	}
}

let studentFour = {
	name: "Jessie",
	email: "jessie@mail.com",
	grades: [91, 89, 92, 93],
	login () {
		console.log(`${this.email} has logged in`);
	},
	logout () {
		console.log(`${this.email} has logged out`);
	},
	listGrades () {
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
	},
	computeAve (){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		return sum/(this.grades.length)
	},
	willPass (){
		return this.computeAve() >= 85;
	},
	willPassWithHonors (){
		if (this.computeAve() >= 90){
			return true
		} else if (this.computeAve() >= 85) {
			return false
		} else {
			return undefined
		}
	}
}


// 5. - 9.

	let classOf1A = {
		students: [studentOne, studentTwo, studentThree, studentFour],
		countHonorStudents () {
			let count = 0
 			for (i=0; i < this.students.length; i++) {
 				if (this.students[i].willPassWithHonors() === true){
 					count++
 				}
 			}
 			return count
		},
		honorsPercentage () {
			return (this.countHonorStudents()/this.students.length)*100
		},
		retrieveHonorStudentInfo () {
			let honorStudents = []
			for (i=0; i < this.students.length; i++) {
 				if (this.students[i].willPassWithHonors() === true){
 					honorStudents.push({
 						aveGrade: this.students[i].computeAve(),
 						email: this.students[i].email
 					})
 				}
 			}
 			return honorStudents
		},
		sortHonorStudentsByGradeDesc () {
			return this.retrieveHonorStudentInfo().sort((a,b) => b.aveGrade - a.aveGrade);

		}

	}
